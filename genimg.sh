#!/bin/bash

# checks
## check if imagicmagic is installed 
if ! hash convert 2>/dev/null; then
	echo "Please install imagemagic."
	exit 127 # command not found error
fi
## Check if argument is set
PORTALHEX=$1
if [ -z "$PORTALHEX" ]; then
    echo "PORTAL HEX VALUE REQUIRED"
    exit 1
fi
IMGSIZE=$2
if [ -z "$IMGSIZE" ]; then
    echo -e "IMAGE SIZE REQUIRED\n3072x256\t1536x126\t768x64\t384x32\t192x16\nDefaulting to 768x64"
    IMGSIZE="768x64"
fi
SAVENAME=$3
if [ -z "$SAVENAME" ]; then
    echo -e "PLANET NAME REQUIRED"
    exit 1
fi

## check if image size is valid
case $IMGSIZE in
    3072x256|XL|xl)
        IMGSIZE="3072x256"
        ;;
    1536x126|L|l)
        IMGSIZE="1536x126"
        ;;
    768x64|M|m)
        IMGSIZE="768x64"
        ;;
    384x32|S|s)
        IMGSIZE="384x32"
        ;;
    192x16|T|t)
        IMGSIZE="192x16"
        ;;
    *)
        echo -e "Invalid image size\n3072x256\t1536x126\t768x64\t384x32\t192x16\nDefaulting to 768x64"
        IMGSIZE="768x64"
        ;;
esac

## Check if char len of is exactly 12 chars long
PHLEN=${#PORTALHEX}
if [ $PHLEN != 12 ]; then
	echo "Portal Addresses MUST be exactly 12 chars [ A-F|a-f & 0-9 ] long"
    exit 1
fi

# format the command string
## Make string uppercase
PORTALHEX=${PORTALHEX^^}
## Assign each (hex) number a glyph
SFIX=".png"
PFIX="./glyphs/GLYPH-"
G0=$PFIX$(echo $PORTALHEX | cut -c 1)$SFIX
G1=$PFIX$(echo $PORTALHEX | cut -c 2)$SFIX
G2=$PFIX$(echo $PORTALHEX | cut -c 3)$SFIX
G3=$PFIX$(echo $PORTALHEX | cut -c 4)$SFIX
G4=$PFIX$(echo $PORTALHEX | cut -c 5)$SFIX
G5=$PFIX$(echo $PORTALHEX | cut -c 6)$SFIX
G6=$PFIX$(echo $PORTALHEX | cut -c 7)$SFIX
G7=$PFIX$(echo $PORTALHEX | cut -c 8)$SFIX
G8=$PFIX$(echo $PORTALHEX | cut -c 9)$SFIX
G9=$PFIX$(echo $PORTALHEX | cut -c 10)$SFIX
GA=$PFIX$(echo $PORTALHEX | cut -c 11)$SFIX
GB=$PFIX$(echo $PORTALHEX | cut -c 12)$SFIX
## Assemble command string
FMTCMD="convert +append $G0 $G1 $G2 $G3 $G4 $G5 $G6 $G7 $G8 $G9 $GA $GB -resize $IMGSIZE $SAVENAME-$IMGSIZE.png"

# execute command string
bash -c "$FMTCMD"