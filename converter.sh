#!/bin/bash

# CHECKS
## Check if argument is set
PORTALHEX=$1
if [ -z "$PORTALHEX" ]; then
    exit 1
fi

## Check if char len of is exactly 12 chars long
PHLEN=${#PORTALHEX}
if [ $PHLEN != 12 ]; then
	echo "Portal Addresses MUST be exactly 12 chars long"
    exit 1
fi

# Format sting into a hex
SSI=$(echo $PORTALHEX | cut -c 2-4)
Y=$(echo $PORTALHEX | cut -c 5-6)
Z=$(echo $PORTALHEX | cut -c 7-9)
X=$(echo $PORTALHEX | cut -c 10-12)

## Convert Hex to Dec
Yd=$(echo "ibase=16; $Y" | bc)
Xd=$(echo "ibase=16; $X" | bc)
Zd=$(echo "ibase=16; $Z" | bc)

# Begin Algorithem
## this will help deal with the galaxys shape and boundaries (math is done in dec)
if (( $Yd <= 128 )) ; then
    Y=$(expr $Y + 100)
fi
if (( $Zd <= 2048 )) ; then
    Z=$(expr $Z + 1000)
fi
if (( $Xd <= 2048 )) ; then
    X=$(expr $X + 1000)
fi

## convert dec to hex and update variables
Yd=$(echo "ibase=16; $Y" | bc)
Xd=$(echo "ibase=16; $X" | bc)
Zd=$(echo "ibase=16; $Z" | bc)

## Apply hex shift ( Y +0x081 X & Z +0x800 )
Y=$(echo "obase=16; $(expr $Yd - 129)" | bc)
X=$(echo "obase=16; $(expr $Xd - 2049)" | bc)
Z=$(echo "obase=16; $(expr $Zd - 2049)" | bc)

## padd with zeros, each hex chunck will be 4 chars 0000:0000:0000:0000
X=`printf "%04x" 0x$X`
Y=`printf "%04x" 0x$Y`
Z=`printf "%04x" 0x$Z`
SSI=`printf "%04x" 0x$SSI`

## reassemble hex
PORTALHEX="$X:$Y:$Z:$SSI"

echo -e $PORTALHEX
